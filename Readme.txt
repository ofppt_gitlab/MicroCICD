Les étapes:
1- copier le dossier du projet CI içi
2- git init
3- git add .
4- git commit -m "Local project"
5- git remote add origin https://gitlab.com/ofppt_gitlab/MicroCICD.git
6- git push origin master
7- rendre le projer publique à partir de "Settings", "General", "Visibility, project features, permissions"


17- Créer un compte sonarcube à l'aide de GITLABA: https://www.sonarsource.com/products/sonarcloud/signup/
18- appuez sur + : analyse a new project
19- appuez sur:  create a project manually.
20- choisir l'organisation convenable
21- donner votre "Project Key" et "Display Name"
22: Choose your Analysis Method: "With GitLab CI/CD Pipeline"
23: Suivez les instructions "Add environment variables"
24: Dans le fichier ".gitlab-ci.yml" enlever :
    only:
      - merge_requests
      - master
      - develop
25: Dans le fichier ".gitlab-ci.yml" ajouter "stage: test" au niveau service test et en haut ajouter:
  stages:
    - test
    - build
26: Ajouter les variables: REGISTRY_USER et REGISTRY_PASS
27: Le fichier ".gitlab-ci.yml" devient :
stages:
  - test
  - build
MicroCI_test:
  stage: test
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script:
    - sonar-scanner -Dsonar.qualitygate.wait=true
MicroCI_build:
  stage: build
  image: docker
  services:
    - docker:dind
  before_script:
    - docker login -u $REGISTRY_USER -p $REGISTRY_PASS
  script:
    - docker-compose -f microservice/docker-compose.yml  build 
28- corriger le problème de sécurité sur Dockerfile.auth et Dockerfile.receive en ajoutant:
RUN /usr/sbin/addgroup grp \
    && /usr/sbin/adduser --disabled-password --disabled-login \
    --no-create-home --ingroup grp --gecos "" usr

USER usr
29- configurer quality Gate sur la base de previously